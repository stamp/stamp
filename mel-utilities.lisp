;;; mel-utilities

(in-package :stamp-core)

;;; Make a rfc2882 type message from file "file" 

(defclass maildir ()
  ((name :accessor name :type pathname :initarg :name)
   (current-mail :accessor current-mail :type pathname :initarg :current-mail)
   (new-mail :accessor new-mail :initarg :new-mail)
   (tmp-mail :accessor tmp-maim :initarg :tmp-mail)))

(defun  make-maildir-with-tags (pathname) 
  (let* ((name (namestring pathname))
	 (cur (concatenate 'string (namestring pathname) 
			   "cur/"))
	 (new (concatenate 'string (namestring pathname)
			   "new/"))
	 (tmp (concatenate 'string (namestring pathname)
			   "tmp/"))    
	 (folder 
	  (make-instance 'maildir
			 :name name
			 :current-mail cur 
			 :new-mail new 
			 :tmp-mail tmp)))
    (with-open-file 
	(c (ensure-directories-exist cur))
      :direction :output
      :if-does-not-exist :create)
    (with-open-file
	(n (ensure-directories-exist new))
      :direction :output 
      :if-does-not-exist :create)
    (with-open-file
	(tm (ensure-directories-exist tmp))
      :direction :output 
      :if-does-not-exist :create)
  folder))

(defun make-message-from-file (pathname)
  "Return a mime-message from pathname"
  (let ((message (make-instance 'mel:mime-message
				:folder (pathname pathname)
				:uid pathname)))
    (with-open-file (s pathname)
      (setf (mel:header-fields message) (mel:read-rfc2822-header s)))
    message))

(defun delete-message (message)
  "Delete the message"
  (let ((mes (mel:folder message)))
    (delete-file mes)))

(defun get-messages-with-tags (folder l)
  "Return list of messages in folder with the tags list l" 
  (let ((messages nil))
    (flet ((push-message(file)
	     (let ((message (make-message-from-file (concatenate 'string folder file))))
	       (push message messages)
	       message)))
      (dolist (tmp l)
	(push-message tmp)))
    (nreverse messages)))

(defun next-message (message l)
  "Return the next message of message in the list l"
  (let* ((messages nil)
	 (mes nil))
    (setf messages (loop for tmp in l 
		      collect  (mel:folder tmp)))
    (setf mes (next-object-in-sequence (mel:folder message) messages))
    (make-message-from-file  mes)))

(defun previous-message (message l)
  "Return the previous message of message in the list l"
  (let* ((messages nil)
	 (mes nil))
    (setf messages (loop for tmp in l 
		      collect  (mel:folder tmp)))
    (setf mes (previous-object-in-sequence (mel:folder message) messages))
    (make-message-from-file  mes)))

(defun get-all-messages-from-folder(folder)
  "Return the list of all the message in folder"
  (let ((messages nil))
    (flet ((push-message (file)
	     (let ((message (make-message-from-file (concatenate 'string folder file))))
	       (push message messages)
	       message)))
      (mel.filesystem:map-directory 
       (lambda(file)
	 (push-message file))
       folder))
    (nreverse messages)))

(defun get-file-name (pathname)
  "Return the name of the file"
  (let ((start (position #\/ pathname :from-end t)))
    (if start
        (subseq pathname (+ 1 start)  (length pathname))
        pathname)))

(defun move-message (message folder-to)
  "Move message in folder-to"
  (let* ((mes (get-file-name (mel:uid message)))
	(file-from (mel:uid message))
	(file-to   (concatenate 'string folder-to mes)))
    (copy-file file-from file-to)
    (delete-message message)
  mes))


