;;; stamp commands
(in-package :stamp-gui)

(declaim (optimize (debug 3)))

(clim:define-command-table stamp
    :inherit-from (esa:global-esa-table esa:keyboard-macro-table))

(define-stamp-command (com-quit :name t
				:keystroke (#\q :control)) ()
  (clim:frame-exit clim:*application-frame*))

(define-stamp-command (com-select-message :name t)
    ((message 'message :gesture :select))
  (setf (current-message clim:*application-frame*) message)
  (redisplay-pane 'headers-pane)
  (redisplay-pane 'message-pane))

(define-stamp-command (com-select-filter :name t)
    ((filter 'filter :gesture :select))
  (dolist (tmp (filters clim:*application-frame*))
    (if (equal (string-downcase filter) (string-downcase tmp))
	       (progn
		 (setf (current-filter clim:*application-frame*) (string-downcase filter))
		 (setf stamp-core:*current-filters-mails* 
		       (filter-messages-for-id (find-symbol (string-upcase filter)
							    (find-package "STAMP-CORE"))))
		 (redisplay-pane 'filters-pane)
		 (redisplay-pane 'headers-pane)
		 (redisplay-pane 'message-pane)))))


(define-stamp-command (com-define-filter :name t) ()
 (let ((content-filename (make-temporary-filename)))
    (with-open-file (out content-filename :direction :output)
      (princ "(define-filter <name> (tag ...)
   <body>)"   out))
    (let ((filename (make-temporary-filename)))
      (let ((*climacs-startup-hook*
             (lambda ()
               (clim:layout-frame *climacs-frame* 800 600)
               (clim:execute-frame-command
                *climacs-frame*
                `(climacs-core::find-file ,filename))
               (clim:execute-frame-command
                *climacs-frame*
                `(climacs-commands::com-insert-file ,content-filename))))
            (*climacs-frame*
             (clim:make-application-frame 'climacs-gui:climacs)))
        (clim:run-frame-top-level *climacs-frame*))
      (when (probe-file filename)
	(progn
	  (with-open-file (out (concatenate 'string *config-folder* "default-filters.lisp")
			   :direction :output
			   :element-type 'unsigned-byte
			   :if-exists :append
			   :if-does-not-exist :create)
	    (with-open-file (in filename
			     :direction :input
			     :element-type 'unsigned-byte)
	      (do ((i (read-byte in nil -1)
		      (read-byte in nil -1)))
		  ((minusp i))
		(declare (fixnum i))
		(write-byte i out))))
	  (load-filters)
	  (setf (filters clim:*application-frame*) (load-names))
	  (redisplay-pane 'filters-pane) 
	  (redisplay-pane 'headers-pane))))))

(define-stamp-command (com-next-message :name t
                                        :keystroke (#\n :control)) ()
  (if (not (equal (current-message clim:*application-frame*) nil)) 
      (progn 
	(setf (current-message clim:*application-frame*)
	      (next-message (current-message clim:*application-frame*) *messages*))
	(redisplay-pane 'headers-pane)
	(redisplay-pane 'message-pane))))
 

(define-stamp-command (com-previous-message :name t
                                            :keystroke (#\p :control)) ()
  (if (not (equal (current-message clim:*application-frame*) nil)) 
      (progn 
	(setf (current-message clim:*application-frame*)
	      (previous-message (current-message clim:*application-frame*) *messages*))
	(redisplay-pane 'headers-pane)
	(redisplay-pane 'message-pane))))
  
;;; Remake this command with the filters system
(define-stamp-command (com-delete-message :name t
                                          :keystroke (#\d :control)) ()
  (let ((message (current-message clim:*application-frame*)))
    (unless (null message)
      (mel:delete-message message)))
 (setf (current-message clim:*application-frame*) nil)
  (redisplay-pane 'headers-pane)
  (redisplay-pane 'message-pane))


(define-stamp-command (com-compose-message :name t) ()
  (multiple-value-bind (message headers body)
      (compose-message)
    (when message
      (send-message message headers body))))

(define-stamp-command (com-reply :name t) ()
  (let ((original-message (current-message clim:*application-frame*)))
    (when original-message
      (multiple-value-bind (message headers body)
          (let ((sender (mel:address-spec (mel:from original-message))))
            (compose-message :to sender
                             :subject (format nil "Re: ~A"
                                              (mel:subject original-message))
                             :body (quote-message-text (get-body-string
                                                        original-message)
                                                       sender)))
        (when message
          (send-message message headers body))))))

(define-stamp-command (com-forward :name t) ()
  (let ((original-message (current-message clim:*application-frame*)))
    (when original-message
      (multiple-value-bind (message headers body)
          (let ((sender (mel:address-spec (mel:from original-message))))
            (compose-message :subject (format nil "Fwd: ~A"
                                              (mel:subject original-message))
                             :body (quote-message-text (get-body-string
                                                        original-message)
                                                       sender)))
        (when message
          (send-message message headers body))))))

;;; This command  move the mails from the new-mail to the current-mail directory 
;;; and puts the tags on the files tags.
(define-stamp-command (com-get-mail :name t) ()
 (loop for message in (get-all-messages-from-folder (new-mail *inbox-folder*))
    do 
      (copy-message-and-process-standard-tags message *inbox-folder*)
      (load-tags)
      (setf *current-filters-mails* (filter-messages-for-id (find-symbol (string-upcase (current-filter clim:*application-frame*))(find-package "STAMP-CORE"))))
      (redisplay-pane 'headers-pane)))

(define-stamp-command (com-show-all-headers :name t) ()
  (setf *show-all-headers* (not *show-all-headers*))
  (redisplay-pane 'message-pane))

(define-stamp-command (com-save-attached-file)
    ((part 'attached-file :gesture :select))
  (let ((filename (concatenate 'string
                               (namestring (user-homedir-pathname))
                               (get-attached-file-name part))))
    ;;; Avoid warnings.
    (declare (ignore filename))))
