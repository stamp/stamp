;;; misc-utilities

(in-package :stamp-core)

(declaim (optimize (debug 3)))

(defun format-datetime (time)
  (multiple-value-bind (second minute hour date month year day daylight-p zone)
      (decode-universal-time time)
    (declare (ignore day daylight-p zone))
    (format nil "~4,'0D-~2,'0D-~2,'0D ~2,'0D:~2,'0D:~2,'0D"
            year month date hour minute second)))

(defun capitalize-words (string)
  (with-output-to-string (stream)
    (loop with previous-char-alphanumeric = nil
       for c across string
       do (write-char (if (alphanumericp c)
			  (if previous-char-alphanumeric
			      (char-downcase c)
			      (char-upcase c))
			  c)
		      stream)
	 (setf previous-char-alphanumeric (alphanumericp c)))))

(defun read-stream-as-string (stream)
  (with-output-to-string (string-stream)
    (loop for c = (read-char stream nil nil)
       until (null c)
       unless (char= c #\return)
       do (write-char c string-stream))))

(defun next-object-in-sequence (object sequence &key (test #'equal))
  (let ((length (length sequence))
        (position (position object sequence :test test)))
    (nth (if (= position (1- length)) position (1+ position))
         sequence)))

(defun previous-object-in-sequence (object sequence &key (test #'equal))
  (let ((position (position object sequence :test test)))
    (nth (if (zerop position) 
	     position 
	     (1- position))
         sequence))) 



