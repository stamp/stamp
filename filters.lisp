
(in-package :stamp-core)

(declaim (optimize (debug 3)))

;;; List of the present messages in the file tags1.
(defparameter *tags* '())

(defun load-tags ()
  (setf *tags* (read-file-to-list 
		(concatenate 'string 
			     *mail-folder* 
			     "tags1"))))

;;; List of the functions of filters.
(defparameter *filter-names* '())

;;; Macro for the definition of filters.
(defmacro define-filter (name args &body body)
  `(progn (defun ,name (&rest tags &key ,@args &allow-other-keys)
	    (declare (ignorable tags))
	    ,@body)
	  (compile ',name)))

;;; To get back the filtered messages.
(defun filter-messages (filter)
  (loop for tag in *tags*
       when (apply filter (cdr tag))
       collect tag))

;;; To get back the id (as "1167748389.13017_80.localhost") 
;;; of filtered messages.
(defun filter-messages-for-id (filter)
  (loop for tag in *tags*
       as message-id = (car tag)
       when (apply filter (cdr tag))
       collect message-id))

;;; To get back the list of the names of filters.
(defun load-names ()
  (setf *filter-names*    
	(read-file-to-list
	 (concatenate 'string
		      *config-folder*
		      "default-filters.lisp")))
  (loop for filter in *filter-names*
       as filter-name = (cadr filter)
       when (not (eq filter-name ':stamp-core)) 
       collect (string filter-name)))
