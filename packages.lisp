;;; -*- Mode: Lisp; Package: COMMON-LISP-USER -*-

;;; Copyright (C) 2006  Matthieu Villeneuve (matthieu.villeneuve@free.fr)
;;; Copyright (C) 2006  Robert Strandh (strandh@labri.fr)

;;; This program is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.
;;; 
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;; 
;;; You should have received a copy of the GNU General Public License
;;; along with this program; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

;;; Stamp package definitions

(in-package :cl-user)

(defpackage :stamp-core
  (:use :cl)
  (:export #:stamp
	   ;;; Stamp.
           #:set-smtp-parameters
           #:add-pop3-mailbox

	   #:set-mail-folder
	   #:current-mail
	   #:new-mail
	   #:tmp-mail

	   #:load-filters
	   #:get-body-string
	   #:get-attached-file-name
	   #:copy-message-and-process-standard-tags

	   #:*mail-folder*
	   #:*inbox-folder*
	   #:*config-folder*
	   #:*current-filters-mails*
	   #:*messages*

	   ;;; Files Utilities
	   #:copy-file
	   #:read-file-to-list
	   #:compare-tag-files

	   ;;; Filter
	   #:filter-messages
	   #:filter-messages-for-id
	   #:load-names
	   #:load-tags
	   #:*tags*
	   #:*default-filter*

	   ;;; Mel Utilities
	   #:get-messages-with-tags
	   #:next-message
	   #:previous-message
	   #:get-all-messages-from-folder

	   ;;; Misc Utilities
	   #:format-datetime
	   #:capitalize-words 
	   #:read-stream-as-string
	   #:next-object-in-sequence
	   #:previous-object-in-sequence

	   ;;; ???
	   #:set-default-filter
	   #:*default-filter*))
	
(defpackage :stamp-gui
  (:use :cl :stamp-core)
  (:export #:redisplay-pane
	   #:print-fixed-width-string
	   #:print-properties-as-table
	   #:hilight-line

	   #:*climacs-frame*
	   #:*climacs-startup-hook*
	   
	   ;;; Message Composing
	   #:compose-message
	   #:send-message
	   #:*outbox*
	   #:*mailboxes*
	   #:*address*

           ;;; Config
	   #:*taille-height-m*
           #:*taille-height-m-max*
           #:*taille-height-m-min*
	   #:*width-mbp*
	   #:*width-mbp-max*
	   #:*width-mbp-min*
	   #:*height-mbp*
	   #:*height-mbp-max*
	   #:*height-mbp-min*
	   #:*height-ip*
	   #:*height-ip-max*
	   #:*height-ip-min*
	   #:*width-fp*
	   #:*width-fp-max*
	   #:*width-fp-min*
	   #:*width-sp*
	   #:*width-sp-max*
	   #:*width-sp-min*
	   #:*width-hp*
	   #:*height-hp*
	   #:*height-mp*
	   #:set-size-mbp
	   #:set-size-height-ip
	   #:set-size-width-fp
	   #:set-size-width-sp
	   #:set-size-hp
	   #:set-size-height-mp))
