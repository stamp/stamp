;;; stamp-gui
(in-package :stamp-gui)

(declaim (optimize (debug 3)))

;;; stamp-gui parameters

(defparameter *show-all-headers* nil)

;;; stamp classes

(defclass filters-pane (esa:esa-pane-mixin clim:application-pane) ())
(defclass headers-pane (esa:esa-pane-mixin clim:application-pane) ())
(defclass message-pane (esa:esa-pane-mixin clim:application-pane) ())

(defclass stamp-minibuffer-pane (esa:minibuffer-pane)
  ()
  (:default-initargs
      :height *height-mbp* :max-height *height-mbp-max* :min-height *height-mbp-min*))

(defclass stamp-info-pane (esa:info-pane)
  ()
  (:default-initargs
      :height *height-ip* :max-height *height-ip-max* :min-height *height-ip-min*
      :display-function 'display-info
      :incremental-redisplay t))

;;; display functions

(defun display-info (frame pane)
  (format pane "Filter: ~a" (string-downcase (current-filter frame))))

(clim:define-application-frame stamp (esa:esa-frame-mixin
				      clim:standard-application-frame)
  (
   (filters :initform (load-names)
	    :accessor filters)
   (current-filter :initform nil :accessor current-filter)
   (current-message :initform nil :accessor current-message))
  (:panes (filters-pane (clim:make-pane 'filters-pane
					:display-function '(display-filters)
					:display-time nil
					:width *width-fp* :min-width *width-fp-min* :max-width *width-fp-max* ))
          (headers-pane (let ((pane (clim:make-pane 'headers-pane
						    :display-function '(display-headers)
						    :display-time nil
						    :command-table 'stamp
						    :width *width-hp* :height *height-hp*)))
			  (setf (esa:windows clim:*application-frame*)
				(list pane))
			  pane))
          (message-pane (clim:make-pane 'message-pane
					:display-function '(display-message)
					:display-time nil
					:height *height-mp*))
          (adjuster1 (clim:make-pane 'clim-extensions:box-adjuster-gadget))
          (adjuster2 (clim:make-pane 'clim-extensions:box-adjuster-gadget))
	  (info (clim:make-pane 'stamp-info-pane))
	  (minibuffer (clim:make-pane 'stamp-minibuffer-pane :width *width-mbp*)))
  (:layouts (default-layout
                (clim:vertically ()
                  (clim:horizontally ()
                    (clim:scrolling (:width *width-sp* :min-width *width-sp-min* :max-width *width-sp-max*)
		      filters-pane)
                    adjuster1
                    (clim:vertically ()
                      (clim:scrolling (:width *width-hp* :height *height-hp*) headers-pane)
                      adjuster2
                      (clim:scrolling (:height *height-mp*) message-pane)))
		  info
		  minibuffer)))
  (:top-level (esa:esa-top-level)))


(defmethod clim:adopt-frame :after (frame-manager (frame stamp))
  (setf (current-filter frame) *default-filter*))


(defmethod clim:handle-event :after ((pane clim-internals::composite-pane)
                                     (event clim:window-configuration-event))
  (when (eq (clim:frame-name (clim:pane-frame pane)) 'stamp)
    (redisplay-pane 'headers-pane)
    (redisplay-pane 'message-pane)))


(clim:define-presentation-type filter ())

(clim:define-presentation-type message ())

(clim:define-presentation-type attached-file ())

(defun display-filters (frame pane)
  (clim:with-text-family (pane :sans-serif)
    (clim:with-text-face (pane :bold)
      (write-string "Filters" pane))
    (terpri pane)
    (loop with current-filter = (current-filter frame)
	 for filter in (filters frame)
       do (when (equal (string-downcase filter) current-filter)
	    (multiple-value-bind (cursor-x cursor-y)
		(clim:stream-cursor-position pane)
	      (declare (ignore cursor-x))
	      (hilight-line pane cursor-y)))
	 (write-string "  " pane)
	 (clim:with-output-as-presentation (pane filter 'filter)               
	   (write-string (string-downcase filter) pane))
	 (terpri pane))))

(defun display-headers (frame pane)
  (clim:with-text-family (pane :sans-serif)
    (setf *messages* (sort (copy-list (get-messages-with-tags 
				       (current-mail *inbox-folder*) 
				       *current-filters-mails*))
                           #'< :key #'mel:date))
	  
	 (let*  ((current-message (current-message frame))
           (pane-region (clim:pane-viewport-region pane))
           (pane-width (- (clim:bounding-rectangle-width pane-region) 20))
           (index-width (clim:stream-string-width
                         pane
                         (princ-to-string (length *messages*))))
           (date-width (min (clim:stream-string-width pane
                                                      "0000-00-00 00:00:00")
                            (floor (* pane-width 0.25))))
           (subject-width (floor (* pane-width 0.5)))
           (from-width (- pane-width index-width subject-width date-width 10)))
      (clim:with-text-face (pane :bold)
        (print-fixed-width-string pane "" (+ index-width 10))
        (print-fixed-width-string pane "Subject" subject-width)
        (print-fixed-width-string pane "From" from-width)
        (print-fixed-width-string pane "Date" date-width)
        (terpri pane))
      (loop for message in *messages*
            for index from 1
            do (when (and (not (eq current-message nil))
			  (equal (mel:uid message) (namestring (mel:uid current-message))))
                 (multiple-value-bind (cursor-x cursor-y)
                     (clim:stream-cursor-position pane)
                   (declare (ignore cursor-x))
                   (hilight-line pane cursor-y)))
	
               (print-fixed-width-string pane
                                         (princ-to-string index)
                                         index-width
                                         :align :right)
               (print-fixed-width-string pane "" 10)
               (clim:with-output-as-presentation (pane message 'message)
                 (print-fixed-width-string pane
                                           (remove #\newline
                                                   (mel:subject message))
                                           subject-width))
               (print-fixed-width-string pane
                                         (mel:address-spec (mel:from message))
                                         from-width)
               (print-fixed-width-string pane
                                         (format-datetime (mel:date message))
					 date-width)
	   (terpri pane)))))

(defun display-message (frame pane)
  (let ((message (current-message frame)))
    (when message
      (clim:with-text-family (pane :sans-serif)
        (print-properties-as-table
         pane
         (if *show-all-headers*
             (mapcar (lambda (header)
                       (let ((name (symbol-name (car header))))
                         (cons (capitalize-words name)
                               (string-trim " " (cdr header)))))
                     (mel:header-fields message))
             (list (cons "From:" (mel:address-spec (mel:from message)))
                   (cons "Date:" (format-datetime (mel:date message)))
                   (cons "Subject:" (mel:subject message))))))
      (terpri pane)
      (if (eq (mel:content-type message) :multipart)
          (display-multipart-body pane message)
          (write-string (get-body-string message) pane))
      (terpri pane))))

(defun display-multipart-body (pane message)
  (let ((text-parts '())
        (attached-parts '()))
    (loop for part in (mel:parts message)
          do (multiple-value-bind (super-type sub-type)
                 (mel:content-type part)
               (declare (ignore sub-type))
               (if (eq super-type :text)
                   (let ((length (mel:content-octets part)))
                     (push (get-body-string part length) text-parts))
                   (let ((name (get-attached-file-name part)))
                     (unless (null name)
                       (push part attached-parts))))))
    (loop for part in (reverse attached-parts)
          do (clim:with-text-family (pane :sans-serif)
               (clim:with-output-as-presentation (pane part 'attached-file)
                 (write-string (get-attached-file-name part) pane)))
             (terpri pane)
             (terpri pane))
    (loop for part in (reverse text-parts)
          do (write-string part pane))
    (terpri pane)))

