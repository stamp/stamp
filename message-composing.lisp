;;; message-composing

(in-package :stamp-gui)

(declaim (optimize (debug 3)))

(defparameter *outbox* '())
(defparameter *mailboxes* '())
(defparameter *address* '())

(defparameter *climacs-frame* nil)
(defparameter *climacs-startup-hook* nil)

(defmethod clim:adopt-frame :after (frame-manager (frame climacs-gui:climacs))
  (when *climacs-startup-hook*
    (funcall *climacs-startup-hook*)))

(defun compose-message (&key (to "") (subject "") body)
  (let ((content-filename (make-temporary-filename)))
    (with-open-file (out content-filename :direction :output)
      (princ (make-message-file-contents :to to
                                         :subject subject
                                         :body body)
             out))
    (let ((filename (make-temporary-filename)))
      (let ((*climacs-startup-hook*
             (lambda ()
               (clim:layout-frame *climacs-frame* 800 600)
               (clim:execute-frame-command
                *climacs-frame*
                `(climacs-core::find-file ,filename))
               (clim:execute-frame-command
                *climacs-frame*
                `(climacs-commands::com-insert-file ,content-filename))
               (delete-file content-filename)))
            (*climacs-frame*
             (clim:make-application-frame 'climacs-gui:climacs)))
        (clim:run-frame-top-level *climacs-frame*))
      (let ((parsed-data (ignore-errors (parse-message-file filename))))
        (when (probe-file filename)
          (delete-file filename))
        (values (first parsed-data)
                (second parsed-data)
                (third parsed-data))))))



;;; This should be a defconstant, but it is not very 
;;; practical during development, because of the number
;;; of times the file gets reloaded.  -- RS 2007-01-04
(defparameter +boundary+ "---- text follows this line ----")

(defun make-temporary-filename ()
  (let ((base (format nil "/tmp/stamp-~A" (get-universal-time))))
    (loop for i from 0
          as path = (format nil "~A-~A" base i)
          while (probe-file path)
          finally (return path))))

(defun make-message-file-contents (&key (to "") (subject "") body)
  (with-output-to-string (out)
      (format out "To: ~A~%" to)
      (format out "Subject: ~A~%" subject)
      (format out "~A~%" +boundary+)
      (when body
        (princ body out))))

(defun parse-message-file (filename)
  (let* ((string (with-open-file (stream filename)
                   (read-stream-as-string stream)))
         (boundary-position (search +boundary+ string)))
    (when boundary-position
      (let* ((headers (parse-headers string 0 boundary-position))
             (to (cdr (assoc :to headers)))
             (body (string-trim '(#\space #\return #\linefeed)
                                (subseq string (+ boundary-position
                                                  (length +boundary+))))))
        (when to
          (let ((message
                 (mel:make-message :subject (cdr (assoc :subject headers))
                                   :from *address*
                                   :to (cdr (assoc :to headers))
                                   :body body)))
            (setf (mel:header-fields message) headers)
            (list message headers body)))))))

(defun parse-headers (string start end)
  (let ((lines (mapcar (lambda (line)
                         (string-trim '(#\space #\return) line))
                       (split-sequence:split-sequence #\newline string
                                                      :start start
                                                      :end end))))
    (loop for line in lines
          as index = (position #\: line)
          unless (null index)
          collect (cons (intern (string-upcase (subseq line 0 index)) :keyword)
                        (string-trim '(#\space) (subseq line (1+ index)))))))

(defun print-headers (headers stream)
  (loop for header in headers
        as name = (symbol-name (car header))
        do (format stream "~A: ~A~%" (capitalize-words name) (cdr header))))

(defun quote-message-text (text author)
  (let ((lines (mapcar (lambda (line)
                         (string-trim '(#\space #\return) line))
                       (split-sequence:split-sequence #\newline text))))
    (with-output-to-string (out)
      (when author
        (format out "~A wrote:~%" author))
      (loop for line in lines
            do (format out "> ~A~%" line)))))

(defun send-message (message headers body)
  (let ((stream (mel:open-message-storing-stream *outbox* message)))
    (unwind-protect
         (progn
           (print-headers headers stream)
           (format stream body))
      (close stream))))

