;;; clim-utilities

(in-package :stamp-gui)

(declaim (optimize (debug 3)))

(defparameter *hilight-color* (clim:make-rgb-color 0.8 0.8 1.0))

(defun redisplay-pane (name)
  (let ((pane (clim:get-frame-pane clim:*application-frame* name)))
    (clim:redisplay-frame-pane clim:*application-frame* pane :force-p t)))

(defun print-fixed-width-string (pane string width &key (align :left))
  (let* ((string2 (maybe-cut-string-at-width pane string width))
         (string2-width (clim:stream-string-width pane string2)))
    (multiple-value-bind (cursor-x cursor-y)
        (clim:stream-cursor-position pane)
      (setf (clim:stream-cursor-position pane)

            (values (case align
                      (:left cursor-x)
                      (:center (+ cursor-x (floor (- width string2-width) 2)))
                      (:right (+ cursor-x (- width string2-width))))
                    cursor-y))
      (write-string string2 pane)
      (setf (clim:stream-cursor-position pane)
            (values (+ cursor-x width) cursor-y)))))

(defun maybe-cut-string-at-width (pane string max-width)
  (loop for index downfrom (length string)
        as string2 = (if (= index (length string))
                         string
                         (concatenate 'string (subseq string 0 index) "..."))
        as string2-width = (clim:stream-string-width pane string2)
        until (<= string2-width max-width)
        finally (return string2)))

(defun print-properties-as-table (pane properties)
  (clim:formatting-table (pane :x-spacing 10)
    (loop for property in properties
          do (clim:formatting-row (pane)
               (clim:with-text-face (pane :bold)
                 (clim:formatting-cell (pane :align-x :right)
                   (write-string (car property) pane)))
               (clim:formatting-cell (pane)
                 (write-string (cdr property) pane))))))

(defun hilight-line (pane y)
  (multiple-value-bind (pane-x1 pane-y1 pane-x2 pane-y2)
      (clim:bounding-rectangle* pane)
    (declare (ignore pane-y1 pane-y2))
    (let ((height (clim:text-style-height clim:*default-text-style* pane)))
      (clim:draw-rectangle* pane
                            pane-x1 y pane-x2 (+ y height 1)
                            :filled t :ink *hilight-color*))))








