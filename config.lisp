(in-package :stamp-gui)

(declaim (optimize (debug 3)))

;; Size of minibuffer's pane 
;; width
(defparameter *width-mbp* 900)
;; max width
(defparameter *width-mbp-max* 900)
;; min width
(defparameter *width-mbp-min* 900)
;; height
(defparameter *height-mbp* 20)
;; max height
(defparameter *height-mbp-max* 20)
;; min height
(defparameter *height-mbp-min* 20)

;; Size of informations' pane
(defparameter *height-ip* 20)
;; max height
(defparameter *height-ip-max* 20)
;; min height
(defparameter *height-ip-min* 20)


;; Size of filters' pane
;; width
(defparameter *width-fp* 150)
;; max width
(defparameter *width-fp-max* 350)
;; min width
(defparameter *width-fp-min* 350)


;; Size of scroll's pane
;; width
(defparameter *width-sp* 150)
;; max width
(defparameter *width-sp-max* 150)
;; min width
(defparameter *width-sp-min* 150)


;; Size of headers' pane
;; width
(defparameter *width-hp* 800)
;; max width
(defparameter *width-hp-max* 800)
;; min width
(defparameter *width-hp-min* 800)
;; height
(defparameter *height-hp* 450)
;; max height
(defparameter *height-hp-max* 450)
;; min height
(defparameter *height-hp-min* 450)


;; Size of messages' pane
;; height
(defparameter *height-mp* 450)
;; max height
(defparameter *height-mp-max* 450)
;; min height
(defparameter *height-mp-min* 450)

;; default filter
(defparameter *default-filter* "unread")

;; default mail directory
(defparameter *mail-folder* 
  (concatenate 'string 
	       (namestring (user-homedir-pathname)) "Mail/inbox/"))

;; Function for update panes' size

(defun set-size-mbp (h_mbp w_mbp)
  "Resize of the minibuffer's pane"
  (if (< w_mbp *width-mbp-min*)
      (setq *width-mbp* *width-mbp-min*)
      (if (> w_mbp *width-mbp-max*)
	  (setq *width-mbp* *width-mbp-max*)
	  (setq *width-mbp* w_mbp)))
  (if (< h_mbp *height-mbp-min*)
      (setq *height-mbp* *height-mbp-min*)
      (if (> h_mbp *height-mbp-max*)
	  (setq *height-mbp* *height-mbp-max*)
	  (setq *height-mbp* h_mbp))))

(defun set-size-height-ip (h_ip)
  "Resize of the information's pane"
  (if (< h_ip *height-ip-min*)
      (setq *height-ip* *height-ip-min*)
      (if (> h_ip *height-ip-max*)
	  (setq *height-ip* *height-ip-max*)
	  (setq *height-ip* h_ip))))

(defun set-size-width-fp (w_fp)
  "Resize of the filters' pane"
    (if (< w_fp *width-fp-min*)
      (setq *width-fp* *width-fp-min*)
      (if (> w_fp *width-fp-max*)
	  (setq *width-fp* *width-fp-max*)
	  (setq *width-fp* w_fp))))

(defun set-size-width-sp (w_sp)
  "Resize of the scroll's pane"
   (if (< w_sp *width-sp-min*)
      (setq *width-sp* *width-sp-min*)
      (if (> w_sp *width-sp-max*)
	  (setq *width-sp* *width-sp-max*)
	  (setq *width-sp* w_sp))))

(defun set-size-hp (w_hp h_hp)
  "Resize of the header's pane"
  (if (< w_hp *width-hp-min*)
      (setq *width-hp* *width-hp-min*)
      (if (> w_hp *width-hp-max*)
	  (setq *width-hp* *width-hp-max*)
	  (setq *width-hp* w_hp)))
  (if (< h_hp *height-hp-min*)
      (setq *height-hp* *height-hp-min*)
      (if (> h_hp *height-hp-max*)
	  (setq *height-hp* *height-hp-max*)
	  (setq *height-hp* h_hp))))

(defun set-size-height-mp (h_mp)
  "Resize of the message's pane"
  (if (< h_mp *height-mp-min*)
      (setq *height-mp* *height-mp-min*)
      (if (> h_mp *height-mp-max*)
	  (setq *height-mp* *height-mp-max*)
	  (setq *height-mp* h_mp))))

;; Function for update the default filter
(defun set-default-filter (filter)
  "Redifine the default filter"
  (loop for f in (load-names)
     when (equal filter (string-downcase f))
     do (setf *default-filter* filter)))

;; Function for update the mail directory
(defun set-mail-folder (dir)
  "Redifine the pathname of the mail folder"
  (setf *mail-folder* (concatenate 'string 
				   (namestring (user-homedir-pathname)) dir)))
