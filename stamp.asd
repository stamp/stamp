;;; -*- Mode: Lisp; Package: COMMON-LISP-USER -*-

;;; Copyright (C) 2006  Matthieu Villeneuve (matthieu.villeneuve@free.fr)
;;; Copyright (C) 2006  Robert Strandh (strandh@labri.fr)

;;; This program is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.
;;; 
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;; 
;;; You should have received a copy of the GNU General Public License
;;; along with this program; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

;;; Stamp system definition

(defpackage :stamp-core.system
  (:use :common-lisp :asdf))



(in-package :stamp-core.system)

(defsystem :stamp-core
  :depends-on (:mcclim :mel-base :climacs :split-sequence)
  :components  ((:file "packages")
                (:file "config" :depends-on ("packages"))
        	(:file "files-utilities")         
      		(:file "filters")
		(:file "misc-utilities")
		(:file "message-composing")
		(:file "mel-extra")
		(:file "mel-utilities" :depends-on("packages"))
		(:file "clim-utilities")
       		(:file "clim-interface" :depends-on ("packages"))
		(:file "clim-commands" :depends-on ("packages"))
		(:file "stamp" :depends-on ("packages"))))
                      





