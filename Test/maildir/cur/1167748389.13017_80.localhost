Return-Path: <stamp-devel-bounces@common-lisp.net>
Delivered-To: online.fr-matthieu.villeneuve@free.fr
Received: (qmail 32434 invoked from network); 31 Dec 2006 06:06:23 -0000
Received: from 80.68.86.115 (HELO common-lisp.net) (80.68.86.115)
  by mrelay5-1.free.fr with SMTP; 31 Dec 2006 06:06:23 -0000
Received: by common-lisp.net (Postfix)
	id EDA4531035; Sun, 31 Dec 2006 01:05:52 -0500 (EST)
Delivered-To: mvilleneuve@common-lisp.net
Received: by common-lisp.net (Postfix, from userid 65534)
	id D640B31041; Sun, 31 Dec 2006 01:05:52 -0500 (EST)
Received: from common-lisp.net (localhost [127.0.0.1])
	by common-lisp.net (Postfix) with ESMTP id 047E731035
	for <mvilleneuve@common-lisp.net>; Sun, 31 Dec 2006 01:05:50 -0500 (EST)
X-Original-To: stamp-devel@common-lisp.net
Delivered-To: stamp-devel@common-lisp.net
Received: by common-lisp.net (Postfix, from userid 65534)
	id 6625731035; Sun, 31 Dec 2006 01:05:49 -0500 (EST)
Received: from iona.labri.fr (iona.labri.fr [147.210.8.143])
	by common-lisp.net (Postfix) with ESMTP id DFB3B21049
	for <stamp-devel@common-lisp.net>; Sun, 31 Dec 2006 01:05:41 -0500 (EST)
Received: from localhost (localhost.localdomain [127.0.0.1])
	by iona.labri.fr (Postfix) with ESMTP id 3AE64101852
	for <stamp-devel@common-lisp.net>; Sun, 31 Dec 2006 07:05:41 +0100 (CET)
X-Virus-Scanned: amavisd-new at labri.fr
Received: from iona.labri.fr ([127.0.0.1])
	by localhost (iona.labri.fr [127.0.0.1]) (amavisd-new, port 10024)
	with LMTP id y5I7di8O9vLb for <stamp-devel@common-lisp.net>;
	Sun, 31 Dec 2006 07:05:39 +0100 (CET)
Received: from serveur5.labri.fr (serveur5.labri.fr [147.210.9.207])
	by iona.labri.fr (Postfix) with ESMTP id 3C1C4101763
	for <stamp-devel@common-lisp.net>; Sun, 31 Dec 2006 07:05:39 +0100 (CET)
Received: by serveur5.labri.fr (Postfix, from userid 1047)
	id CDB7E3ABB0; Sun, 31 Dec 2006 07:05:38 +0100 (CET)
From: Robert Strandh <strandh@labri.fr>
Message-ID: <17815.21298.484324.673931@serveur5.labri.fr>
Date: Sun, 31 Dec 2006 07:05:38 +0100
To: stamp-devel@common-lisp.net
Subject: [stamp-devel] Some initial thoughts
X-BeenThere: stamp-devel@common-lisp.net
X-Mailman-Version: 2.1.5
Precedence: list
List-Id: stamp-devel.common-lisp.net
List-Unsubscribe: <http://common-lisp.net/cgi-bin/mailman/listinfo/stamp-devel>, 
	<mailto:stamp-devel-request@common-lisp.net?subject=unsubscribe>
List-Archive: <http://common-lisp.net/pipermail/stamp-devel>
List-Post: <mailto:stamp-devel@common-lisp.net>
List-Help: <mailto:stamp-devel-request@common-lisp.net?subject=help>
List-Subscribe: <http://common-lisp.net/cgi-bin/mailman/listinfo/stamp-devel>, 
	<mailto:stamp-devel-request@common-lisp.net?subject=subscribe>
Sender: stamp-devel-bounces@common-lisp.net
Errors-To: stamp-devel-bounces@common-lisp.net
X-Spam-Checker-Version: SpamAssassin 3.0.3 (2005-04-27) on clnet
X-Spam-Level: 
X-Spam-Status: No, score=-1.4 required=5.0 tests=AWL,BAYES_00 
	autolearn=unavailable version=3.0.3

1) I have checked in stamp.lisp and a very preliminary specification
   in Spec/spec.text 

2) Using mel-base is going to create some problems.  For one thing,
   mes-base seems to use base-string and base-char a lot, but that
   seems to be incompatible with the kind of strings used by SBCL.
   The solution would be to fix the declarations in mel-base.  Perhaps
   this has already been fixed; I tripped on this problem by using an
   old version of mel-base. 

   Another problem with mel-base is that it can't handle non-ascii
   contents of message headers and message bodies.  The solution to
   this problem seems to be to make mel-base use flexi-streams, to
   scan the header in ascii mode with a default character (as
   flexi-streams allow) of (say) #\?, and to set the encoding of the
   stream for the body to whatever the content-type/charset header
   indicates (with a default of ascii as for the header).  This would
   require some digging into the mel-base code, but I think it can be
   done.  Mel-base is pretty well written.  To get around this problem
   until mel-base is fixed, I suggest using a test folder that has
   only ascii characters in all of the messages.

   We don't want to use the `messages' function on a folder, simply
   because it scans the entire directory and we might have a million
   files in there.  Instead, we must select an individual message
   based on the tags list, and let mel-base scan that individual
   message. 

3) I am working on a list of priorities.  I think I'll post it here
   and leave it open to debate.  In a week or so, we should have
   nailed the priorities so that the students know what to work on and
   in which order. 

4) I still intend to convert the existing Stamp code to use the ESA
   library, but I don't know at which speed this will happen.  It
   would be good to have something running in a week or so.

5) We need to think about file and package structures.  We should
   definitely create an ASDF file (named `stamp.asd'), and a
   `packages.lisp' file.  The main package should be called `stamp',
   and should contain only exported symbols.  Other packages used for
   implementation should :use the stamp package (I learned this trick
   from CLIM).  We need to decide what other packages are required.
   For now, let's say we only need one: stamp-internals, perhaps with
   a nick name of `stampi'.  We should probably have this working in a
   week or so as well.  

Comments?
-- 
Robert Strandh

---------------------------------------------------------------------
Greenspun's Tenth Rule of Programming: any sufficiently complicated C
or Fortran program contains an ad hoc informally-specified bug-ridden
slow implementation of half of Common Lisp.
---------------------------------------------------------------------
_______________________________________________
stamp-devel mailing list
stamp-devel@common-lisp.net
http://common-lisp.net/cgi-bin/mailman/listinfo/stamp-devel

