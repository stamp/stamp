;;; files-utilities

;;; Management of the various files of the system.

(in-package :stamp-core)

(declaim (optimize (debug 3)))

;;; Given two file names, copy the bytes in the
;;; first one to the second one.
(defun copy-file (from to)
  (with-open-file (in from
		      :direction :input
		      :element-type 'unsigned-byte
		      :if-does-not-exist :error
		      :if-exists :overwrite)
    (with-open-file (out to
			 :direction :output
			 :element-type 'unsigned-byte
			 :if-does-not-exist :create
			 :if-exists :overwrite)
      (loop for byte = (read-byte in nil nil)
	    until (null byte)
	    do (write-byte byte out)))))

;;; Read a file and return a list of the top-level in it.
(defun read-file-to-list (file)
  (with-open-file (stream file 
			  :direction :input 
			  :if-does-not-exist :error)
    (loop for l = (read stream nil nil)
	  until (null l)
	  collect l)))

;;; Allows to verify if files tags1 and tags2 are identical.
(defun compare-tag-files (file1 file2)
  (let ((l1 (read-file-to-list file1))
	(l2 (read-file-to-list file2)))
    (equal l1 l2)))
	

