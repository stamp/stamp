;;; -*- Mode: Lisp; Package: stamp-core -*-

;;; Copyright (C) 2005-2006  Matthieu Villeneuve (matthieu.villeneuve@free.fr)
;;; Copyright (C) 2006  Robert Strandh (strandh@labri.fr)
;;; Copyright (C) 2007  Raquel Andia
;;; Copyright (C) 2007  Alexandre Gomez
;;; Copyright (C) 2007  Sebastien Serani
;;; Copyright (C) 2007  Florian Willemain

;;; This program is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.
;;; 
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;; 
;;; You should have received a copy of the GNU General Public License
;;; along with this program; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

;;; Stamp main code

(in-package :stamp-core)

(declaim (optimize (debug 3)))

(defun set-smtp-parameters (host port username password)
  (setf *outbox* (make-instance 'mel:smtp-relay-folder
                                :host host
                                :port port
                                :username username
                                :password password)))

(defun add-pop3-mailbox (host port username password)
  (push (mel:make-pop3-folder :host host
                              :port port
                              :username username
                              :password password)
        *mailboxes*))

(defparameter *inbox-folder* 
  (make-maildir-with-tags *mail-folder*))

(defparameter *config-folder*
  (concatenate 'string
	       (namestring (user-homedir-pathname)) ".clim/stamp/"))

(defparameter *current-filters-mails* '())

(defparameter *messages* nil)

;;; Loading or creating of system's files.
(defun load-sequence ()
  (let ((start (concatenate 'string *config-folder* "start.lisp"))
	(filter (concatenate 'string *config-folder* "default-filters.lisp"))
	(tags1 (concatenate 'string *mail-folder* "tags1"))
	(tags2  (concatenate 'string *mail-folder* "tags2")))
    (with-open-file(f (ensure-directories-exist *config-folder*))
      :direction :output 
      :if-does-not-exist :create)
    
    (if (not (probe-file filter))
  	(copy-file "skeleton" filter))
    
    (if (probe-file start)
	(load start)
	(with-open-file (f1 start :direction :output :if-does-not-exist :create)
	  (princ "(in-package :stamp-gui)" f1)))
    
    (if (or (not (probe-file tags2)) (not (probe-file tags1)))
	(progn 
	  (with-open-file (f1 tags2 :direction :output :if-does-not-exist :create :if-exists nil))
	  (with-open-file (f2 tags1 :direction :output :if-does-not-exist :create :if-exists nil))))
	    
	(compare-tag-files tags1 tags2)))
	

;;; Filters loading.	
(defun load-filters ()
  (load (concatenate 'string *config-folder* "default-filters.lisp"))
    (if (eq *current-filters-mails* nil)
	(setf *current-filters-mails* (filter-messages-for-id 
				       (find-symbol (string-upcase *default-filter*)
						    (find-package "STAMP-CORE"))))))
     
(defun get-body-string (message &optional length)
  (with-output-to-string (out)
    (with-open-stream (stream (mel:message-body-stream message))
      (loop for c = (read-char stream nil nil)
            for count from 0
            until (or (null c)
                      (and length (>= count length)))
            unless (char= c #\return)
            do (write-char c out)))))

(defun get-attached-file-name (part)
  (multiple-value-bind (super-type sub-type properties)
      (mel:content-type part)
    (declare (ignore super-type sub-type))
    (second (member :name properties))))

  (defun address-string-or-nil (address)
  (if (null address)
      nil
      (mel:address-spec address)))

(defun standard-tags (message)
  "Return a list of standard tags for the message"
  (list :subject (mel:subject message)
        :date (mel:date message)
        :from (address-string-or-nil (mel:from message))
        :to (mapcar #'address-string-or-nil (mel:to message))
        :sender (address-string-or-nil (mel:sender message))
	:unread t))

(defun copy-message-and-process-standard-tags (message folder)
  (let ((folder-name (name folder))
	(message-name (move-message message (current-mail folder)))
	(tags (standard-tags message)))
    (with-open-file (stream (concatenate 'string folder-name "tags1")
			    :direction :output
			    :if-does-not-exist :create
			    :if-exists :append)
      (print (cons message-name tags) stream))
    (with-open-file (stream (concatenate 'string folder-name "tags2")
			    :direction :output
			    :if-does-not-exist :create
			    :if-exists :append)
      (print (cons message-name tags) stream))))

;;; Startup
(defun stamp ()
  (if (load-sequence)
      (progn
	(load-tags)
	(load-filters)
	(clim:run-frame-top-level (clim:make-application-frame 'stamp)))
      (print "Error on the tags files")))


















